package stdev.server.stdev;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Karen on 17.07.2017.
 */
@RestController
public class LoginRestController {
    Database database=new Database();


    @RequestMapping(path = "/login")
    public User userData(@RequestParam("login") String login, @RequestParam("password") String password){
        return database.getUserByLoginPassword(login,password);
    }


}
