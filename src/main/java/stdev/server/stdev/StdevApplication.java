package stdev.server.stdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StdevApplication {

	public static void main(String[] args) {
		SpringApplication.run(StdevApplication.class, args);
	}
}
