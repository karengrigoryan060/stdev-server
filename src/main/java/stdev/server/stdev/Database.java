package stdev.server.stdev;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karen on 17.07.2017.
 */
public class Database {

    public User getUserByLoginPassword(String login, String password){
        List<User> userList=new ArrayList<>();
        userList.add(new User("user1","password1"));
        userList.add(new User("user2","password2"));
        userList.add(new User("user3","password3"));
        userList.add(new User("user4","password4"));
        userList.add(new User("user5","password5"));

        for (User user:userList) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

}
